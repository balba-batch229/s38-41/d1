const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},

	description : {
		type: String,
		required: [true, "Descrition is required"]
	},
	price{
		type: Number,
		required:[true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true

	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	enrolless: [{
		userID: {
			type: String,
			required: [true, "UserId is required"]
		},
		enrolledOn : {
			type: Date,
			Date: new Date()
	
		}
	}]


})

module.exports = mongoose.model("Course", courseSchema);