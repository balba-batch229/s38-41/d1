const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")
const userRoutes = require("./routes/user.js")

const app = express();


mongoose.connect("mongodb+srv://admin:admin123@zuitt.cvgjjmq.mongodb.net/s37-s41?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

